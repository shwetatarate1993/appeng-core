"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var inbound_data_controller_1 = require("./inbound.data.controller");
exports.inboundDataController = inbound_data_controller_1.default;
var data_validator_controller_1 = require("./data.validator.controller");
exports.dataValidatorController = data_validator_controller_1.default;
var data_preprocessor_controller_1 = require("./data.preprocessor.controller");
exports.dataPreProcessorController = data_preprocessor_controller_1.default;
//# sourceMappingURL=index.js.map