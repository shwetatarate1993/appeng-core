import { Request, Response } from 'express';
export declare class InboundDataController {
    inboundDataProcess(req: Request, res: Response): Promise<Response>;
}
declare const inboundDataController: InboundDataController;
export default inboundDataController;
//# sourceMappingURL=inbound.data.controller.d.ts.map