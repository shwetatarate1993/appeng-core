"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const info_commons_1 = require("info-commons");
const services_1 = require("../services");
class DataPreProcessorController {
    async dataPreProcess(req, res) {
        const response = await services_1.dataPreProcessor.process(req.get(info_commons_1.TOKEN_KEY), req.get(info_commons_1.USER_DETAILS_KEY), req.body);
        if (response instanceof info_commons_1.ErrorResponse) {
            return res.status(response.code).json(response);
        }
        else {
            return res.status(200).json(response);
        }
    }
}
exports.DataPreProcessorController = DataPreProcessorController;
const dataPreProcessorController = new DataPreProcessorController();
Object.freeze(dataPreProcessorController);
exports.default = dataPreProcessorController;
//# sourceMappingURL=data.preprocessor.controller.js.map