"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const node_mocks_http_1 = __importDefault(require("node-mocks-http"));
const __1 = require("../");
const config_1 = __importDefault(require("../../config"));
const constant_1 = require("../../constant");
const init_config_1 = require("../../init-config");
const mockdata_1 = require("../../mockdata");
init_config_1.AppengCoreConfig.configure(config_1.default.get(constant_1.DB), config_1.default.get(constant_1.LINKS), 'sqlite3');
test('Data PreProcessor Test', async () => {
    const request = node_mocks_http_1.default.createRequest({
        method: 'POST',
        url: 'v1/datapreprocess',
        body: mockdata_1.dataPreprocessorRequestMock,
        headers: mockdata_1.MOKED_HEADER,
    });
    const response = node_mocks_http_1.default.createResponse();
    try {
        await __1.dataPreProcessorController.dataPreProcess(request, response);
        const responseData = JSON.parse(response._getData());
        expect(responseData.formData[0].FAMILIES_WITHOUT_TOILETS === 14).toBe(true);
        expect(responseData.configType === mockdata_1.dataPreprocessorRequestMock.configType).toBe(true);
        expect(responseData.configId === mockdata_1.dataPreprocessorRequestMock.configId).toBe(true);
    }
    catch (error) {
        throw error;
    }
});
//# sourceMappingURL=data.preprocessor.controller.test.js.map