"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const appeng_process_1 = require("appeng-process");
const node_mocks_http_1 = __importDefault(require("node-mocks-http"));
const __1 = require("../");
const config_1 = __importDefault(require("../../config"));
const constant_1 = require("../../constant");
const init_config_1 = require("../../init-config");
const mockdata_1 = require("../../mockdata");
init_config_1.AppengCoreConfig.configure(config_1.default.get(constant_1.DB), config_1.default.get(constant_1.LINKS), 'sqlite3');
const knex = appeng_process_1.knexClient(config_1.default.get('db'), constant_1.MOCK, 'sqlite3');
const deleteTablesExistingRecords = async () => {
    await knex('Company').del();
    await knex('CompanyExpense').del();
    await knex('Employee').del();
    await knex('EmployeeAddress').del();
    await knex('Employee_Address').del();
    await knex('Employee_Contact').del();
};
const createCompanyTable = async () => {
    return knex.schema.createTableIfNotExists('Company', (table) => {
        table.integer('ID');
        table.string('NAME');
        table.string('PLACE');
        table.string('CITY');
        table.string('COUNTRY');
        table.timestamp('CURR_DATE');
        table.integer('EXPENSE');
    });
};
const createCompanyExpenseTable = async () => {
    return knex.schema.createTableIfNotExists('CompanyExpense', (table) => {
        table.integer('ID');
        table.integer('COMPANY_ID');
        table.integer('EXPENSE');
    });
};
const createEmployeeTable = async () => {
    return knex.schema.createTableIfNotExists('Employee', (table) => {
        table.integer('ID');
        table.string('NAME');
        table.integer('COMPANY_ID');
        table.string('COMPANY_NAME');
        table.integer('INCOME');
    });
};
const createEmployeeAddressTable = async () => {
    return knex.schema.createTableIfNotExists('EmployeeAddress', (table) => {
        table.string('ID');
        table.string('ADDRESS');
        table.integer('EMPLOYEE_ID');
        table.string('COMPANY_NAME');
    });
};
const createEmployeeAddressIntTable = async () => {
    return knex.schema.createTableIfNotExists('Employee_Address', (table) => {
        table.integer('ID');
        table.string('ADDRESS');
        table.integer('EMPLOYEE_ID');
        table.string('COMPANY_NAME');
    });
};
const createEmployeeContactTable = async () => {
    return knex.schema.createTableIfNotExists('Employee_Contact', (table) => {
        table.integer('ID');
        table.string('CONTACT_NO');
        table.integer('CONTACT_ID');
    });
};
const tableSetup = async () => {
    await createCompanyTable();
    await createCompanyExpenseTable();
    await createEmployeeTable();
    await createEmployeeAddressTable();
    await createEmployeeAddressIntTable();
    await createEmployeeContactTable();
    await deleteTablesExistingRecords();
};
test('Successful Insert Record', async () => {
    await tableSetup();
    const request = node_mocks_http_1.default.createRequest({
        method: 'POST',
        url: 'v1/submit',
        body: mockdata_1.apiRequestMock,
        headers: mockdata_1.MOKED_HEADER,
    });
    const response = node_mocks_http_1.default.createResponse();
    try {
        await __1.inboundDataController.inboundDataProcess(request, response);
        expect(response.statusCode === 201).toBe(true);
        expect(JSON.parse(response._getData()).pk === 1).toBe(true);
    }
    catch (error) {
        throw error;
    }
});
test('Throw Validation', async () => {
    mockdata_1.apiRequestMock.baseEntity.records[0].ID = null;
    const request = node_mocks_http_1.default.createRequest({
        method: 'POST',
        url: 'v1/submit',
        body: mockdata_1.apiRequestMock,
        headers: mockdata_1.MOKED_HEADER,
    });
    const response = node_mocks_http_1.default.createResponse();
    try {
        await __1.inboundDataController.inboundDataProcess(request, response);
        expect(response.statusCode === 406).toBe(true);
        expect(JSON.parse(response._getData()).errors.length === 1).toBe(true);
    }
    catch (error) {
        throw error;
    }
});
test('New Record Will Insert', async () => {
    mockdata_1.apiRequestMock.baseEntity.records[0].NAME = 'InfoOrigin Tech';
    const request = node_mocks_http_1.default.createRequest({
        method: 'POST',
        url: 'v1/submit',
        body: mockdata_1.apiRequestMock,
        headers: mockdata_1.MOKED_HEADER,
    });
    const response = node_mocks_http_1.default.createResponse();
    try {
        await __1.inboundDataController.inboundDataProcess(request, response);
        expect(response.statusCode === 201).toBe(true);
        expect(JSON.parse(response._getData()).pk === 2).toBe(true);
    }
    catch (error) {
        throw error;
    }
});
test('Successful Update Record', async () => {
    mockdata_1.apiRequestMock.baseEntity.records[0].ID = 1;
    mockdata_1.apiRequestMock.baseEntity.records[0].LOCATION = 'Tilakward,PoonaToly';
    mockdata_1.apiRequestMock.baseEntity.records[0].NAME = 'InfoOrigin';
    const request = node_mocks_http_1.default.createRequest({
        method: 'POST',
        url: 'v1/submit',
        body: mockdata_1.apiRequestMock,
        headers: mockdata_1.MOKED_HEADER,
    });
    const response = node_mocks_http_1.default.createResponse();
    try {
        await __1.inboundDataController.inboundDataProcess(request, response);
        expect(response.statusCode === 201).toBe(true);
        expect(JSON.parse(response._getData()).pk === 1).toBe(true);
    }
    catch (error) {
        throw error;
    }
});
test('Business Rule Test', async () => {
    mockdata_1.apiRequestMock.baseEntity.records[0].ID = 1;
    mockdata_1.apiRequestMock.baseEntity.records[0].LOCATION = 'Tilakward,PoonaToly,Gondia';
    mockdata_1.apiRequestMock.baseEntity.records[0].NAME = 'InfoOrigin';
    const request = node_mocks_http_1.default.createRequest({
        method: 'POST',
        url: 'v1/submit',
        body: mockdata_1.apiRequestMock,
        headers: mockdata_1.MOKED_HEADER,
    });
    const response = node_mocks_http_1.default.createResponse();
    try {
        await __1.inboundDataController.inboundDataProcess(request, response);
        const responseData = JSON.parse(response._getData());
        const updatedRecord = await knex.table('Company').first().where('ID', responseData.pk);
        const companyExpenses = await knex.table('CompanyExpense').select().where('ID', responseData.pk);
        const employeeRecords = mockdata_1.apiRequestMock.baseEntity.childEntities[0].records;
        let employeeIncomeSum = 0;
        employeeRecords.map((emp) => {
            employeeIncomeSum = employeeIncomeSum + emp.INCOME;
        });
        const ruleConcatination = mockdata_1.apiRequestMock.baseEntity.records[0].NAME + ' ' +
            mockdata_1.apiRequestMock.baseEntity.records[0].LOCATION;
        expect(employeeIncomeSum === updatedRecord.EXPENSE).toBe(true);
        expect(ruleConcatination === updatedRecord.PLACE).toBe(true);
        expect(companyExpenses.length === 1).toBe(true);
        expect(companyExpenses[0].COMPANY_ID === updatedRecord.ID).toBe(true);
        expect(companyExpenses[0].EXPENSE === updatedRecord.EXPENSE).toBe(true);
    }
    catch (error) {
        throw error;
    }
});
//# sourceMappingURL=inbound.data.controller.test.js.map