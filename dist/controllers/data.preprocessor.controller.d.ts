import { Request, Response } from 'express';
export declare class DataPreProcessorController {
    dataPreProcess(req: Request, res: Response): Promise<Response>;
}
declare const dataPreProcessorController: DataPreProcessorController;
export default dataPreProcessorController;
//# sourceMappingURL=data.preprocessor.controller.d.ts.map