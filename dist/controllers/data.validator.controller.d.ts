import { Request, Response } from 'express';
export declare class DataValidatorController {
    dbValidator(req: Request, res: Response): Promise<Response>;
}
declare const dataValidatorController: DataValidatorController;
export default dataValidatorController;
//# sourceMappingURL=data.validator.controller.d.ts.map