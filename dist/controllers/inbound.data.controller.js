"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const info_commons_1 = require("info-commons");
const init_config_1 = require("../init-config");
class InboundDataController {
    async inboundDataProcess(req, res) {
        const inboundDataService = init_config_1.AppengCoreConfig.INSTANCE;
        const response = await inboundDataService.process(req.get(info_commons_1.TOKEN_KEY), req.get(info_commons_1.USER_DETAILS_KEY), req.body);
        if (response instanceof info_commons_1.ErrorResponse) {
            return res.status(response.code).json(response);
        }
        else {
            return res.status(201).json(response);
        }
    }
}
exports.InboundDataController = InboundDataController;
const inboundDataController = new InboundDataController();
Object.freeze(inboundDataController);
exports.default = inboundDataController;
//# sourceMappingURL=inbound.data.controller.js.map