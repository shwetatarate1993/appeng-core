"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const appeng_validation_1 = require("appeng-validation");
class DataValidatorController {
    async dbValidator(req, res) {
        const dbValidatorForClient = appeng_validation_1.AppengValidationConfig.DB_VALIDATOR_INSTANCE;
        const errorResponse = await dbValidatorForClient
            .dbValidator(req.body.validation, req.body.data);
        return res.status(200).json(errorResponse);
    }
}
exports.DataValidatorController = DataValidatorController;
const dataValidatorController = new DataValidatorController();
Object.freeze(dataValidatorController);
exports.default = dataValidatorController;
//# sourceMappingURL=data.validator.controller.js.map