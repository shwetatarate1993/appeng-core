export declare class DataPreProcessorService {
    process(authToken: string, userDetailsToken: string, requestBody: any): Promise<any>;
}
declare const dataPreProcessor: DataPreProcessorService;
export default dataPreProcessor;
//# sourceMappingURL=data.preprocessor.service.d.ts.map