"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const appeng_process_1 = require("appeng-process");
const appeng_validation_1 = require("appeng-validation");
const info_commons_1 = require("info-commons");
const cloneDeep_1 = __importDefault(require("lodash/cloneDeep"));
const constant_1 = require("../constant");
const validate_token_1 = require("../utilities/validate.token");
class InboundDataService {
    constructor(inboundProcess) {
        this.inboundProcess = inboundProcess;
    }
    async process(authToken, userDetailsToken, requestBody) {
        console.log('SubmitFormService...');
        let tokenResponse = await validate_token_1.validateToken(authToken);
        if (tokenResponse instanceof info_commons_1.ErrorResponse) {
            tokenResponse.message = constant_1.SESSION_EXPIRED;
            return tokenResponse;
        }
        tokenResponse = await validate_token_1.validateToken(userDetailsToken);
        if (tokenResponse instanceof info_commons_1.ErrorResponse) {
            return tokenResponse;
        }
        const referenceData = cloneDeep_1.default(tokenResponse);
        constant_1.tokenClaims.map((key) => delete referenceData[key]);
        try {
            const entityGroupData = await appeng_process_1.transformToLogicalData(requestBody, referenceData);
            const validator = appeng_validation_1.AppengValidationConfig.INSTANCE;
            const errorResponse = await validator.validateData(entityGroupData);
            if (errorResponse.errors.length !== 0) {
                return errorResponse;
            }
            return await this.inboundProcess.process(entityGroupData);
        }
        catch (error) {
            throw error;
        }
    }
}
exports.InboundDataService = InboundDataService;
let inboundData;
const createInboundCoreProcess = (inboundProcess) => {
    if (inboundData === undefined || inboundData === null) {
        inboundData = new InboundDataService(inboundProcess);
        Object.freeze(inboundData);
    }
    return inboundData;
};
exports.default = createInboundCoreProcess;
//# sourceMappingURL=inbound.data.service.js.map