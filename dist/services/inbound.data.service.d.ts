import { InboundProcess } from 'appeng-process';
export declare class InboundDataService {
    private inboundProcess;
    constructor(inboundProcess: InboundProcess);
    process(authToken: string, userDetailsToken: string, requestBody: any): Promise<any>;
}
declare const createInboundCoreProcess: (inboundProcess: InboundProcess) => InboundDataService;
export default createInboundCoreProcess;
//# sourceMappingURL=inbound.data.service.d.ts.map