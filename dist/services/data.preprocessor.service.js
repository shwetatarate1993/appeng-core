"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const appeng_process_1 = require("appeng-process");
const info_commons_1 = require("info-commons");
const constant_1 = require("../constant");
const validate_token_1 = require("../utilities/validate.token");
class DataPreProcessorService {
    async process(authToken, userDetailsToken, requestBody) {
        let tokenResponse = await validate_token_1.validateToken(authToken);
        if (tokenResponse instanceof info_commons_1.ErrorResponse) {
            tokenResponse.message = constant_1.SESSION_EXPIRED;
            return tokenResponse;
        }
        tokenResponse = await validate_token_1.validateToken(userDetailsToken);
        if (tokenResponse instanceof info_commons_1.ErrorResponse) {
            return tokenResponse;
        }
        const dataPreProcessorService = appeng_process_1.AppengProcessConfig.DATA_PREPROCESSOR_INSTANCE;
        const updatedFormData = await dataPreProcessorService.process(requestBody.formData, requestBody.configId, requestBody.configType);
        if (updatedFormData[constant_1.EXCEPTION_KEY]) {
            const errorResponse = new info_commons_1.ErrorResponse();
            errorResponse.code = 417;
            errorResponse.message = constant_1.EXCEPTION_IN_PREPROCESSOR;
            errorResponse.errors = [{ reason: String(updatedFormData[constant_1.EXCEPTION_KEY]) }];
            return errorResponse;
        }
        const successResponse = {};
        successResponse.formData = updatedFormData;
        successResponse.configId = requestBody.configId;
        successResponse.configType = requestBody.configType;
        return successResponse;
    }
}
exports.DataPreProcessorService = DataPreProcessorService;
const dataPreProcessor = new DataPreProcessorService();
Object.freeze(dataPreProcessor);
exports.default = dataPreProcessor;
//# sourceMappingURL=data.preprocessor.service.js.map