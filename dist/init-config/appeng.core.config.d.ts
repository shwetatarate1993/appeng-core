import { InboundDataService } from '../services';
export default class AppengCoreConfig {
    static readonly INSTANCE: InboundDataService;
    static configure(config: any, links: any, configuredDialect: string): void;
    private static inboundService;
    private constructor();
}
//# sourceMappingURL=appeng.core.config.d.ts.map