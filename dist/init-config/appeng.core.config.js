"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const appeng_process_1 = require("appeng-process");
const appeng_validation_1 = require("appeng-validation");
const services_1 = require("../services");
class AppengCoreConfig {
    constructor() {
        /** No Op */
    }
    static get INSTANCE() {
        return AppengCoreConfig.inboundService;
    }
    static configure(config, links, configuredDialect) {
        appeng_process_1.AppengProcessConfig.configure(config, links, configuredDialect);
        appeng_validation_1.AppengValidationConfig.configure(config, configuredDialect);
        const inboundService = services_1.createInboundCoreProcess(appeng_process_1.AppengProcessConfig.INSTANCE);
        AppengCoreConfig.inboundService = inboundService;
    }
}
exports.default = AppengCoreConfig;
//# sourceMappingURL=appeng.core.config.js.map