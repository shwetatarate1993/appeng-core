"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const controllers_1 = require("../controllers");
const router = express_1.default.Router();
router.post('/datapreprocess', controllers_1.dataPreProcessorController.dataPreProcess);
exports.default = router;
//# sourceMappingURL=data.preprocessor.route.js.map