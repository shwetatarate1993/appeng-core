"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const data_preprocessor_route_1 = __importDefault(require("./data.preprocessor.route"));
const data_validator_route_1 = __importDefault(require("./data.validator.route"));
const inbound_data_route_1 = __importDefault(require("./inbound.data.route"));
const router = express_1.default.Router();
router.use('/', inbound_data_route_1.default);
router.use('/', data_validator_route_1.default);
router.use('/', data_preprocessor_route_1.default);
exports.default = {
    router,
};
//# sourceMappingURL=index.js.map