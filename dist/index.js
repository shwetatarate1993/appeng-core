"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const info_commons_1 = require("info-commons");
const config_1 = __importDefault(require("./config"));
const constant_1 = require("./constant");
const init_config_1 = require("./init-config");
init_config_1.AppengCoreConfig.configure(config_1.default.get(constant_1.DB), config_1.default.get(constant_1.LINKS), info_commons_1.DIALECT);
exports.inboundService = init_config_1.AppengCoreConfig.INSTANCE;
var services_1 = require("./services");
exports.dataPreProcessor = services_1.dataPreProcessor;
exports.InboundDataService = services_1.InboundDataService;
exports.DataPreProcessorService = services_1.DataPreProcessorService;
//# sourceMappingURL=index.js.map