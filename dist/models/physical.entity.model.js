"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class PhysicalEntity {
    constructor() {
        this.getPropertyMap = () => {
            const dbCodeList = [];
            const physicalColumnMandatoryList = [];
            this.physicalColumns.map((pc) => {
                dbCodeList.push(pc.dbCode);
                if (pc.isPhysicalColumnMandatory) {
                    physicalColumnMandatoryList.push(pc.dbCode);
                }
            });
            return { dbCodeList, physicalColumnMandatoryList };
        };
    }
}
exports.default = PhysicalEntity;
//# sourceMappingURL=physical.entity.model.js.map