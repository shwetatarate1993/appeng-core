export default class ValidationResponse {
    readonly fieldId: string;
    readonly value: any;
    readonly errorMessages: string[];
    constructor(fieldId: string, value: any, errorMessages: string[]);
}
//# sourceMappingURL=validation.response.model.d.ts.map