import { LogicalEntity, NodeBusinessRule, PhysicalEntity } from './';
export default class CompositeEntityNode {
    readonly configObjectId: string;
    readonly entity: LogicalEntity;
    readonly physicalDataEntities: PhysicalEntity[];
    readonly nodeBusinessRules: NodeBusinessRule[];
}
//# sourceMappingURL=composite.entity.node.model.d.ts.map