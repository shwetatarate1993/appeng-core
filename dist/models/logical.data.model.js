"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class LogicalData {
    constructor(node, appData, childData) {
        this.appData = appData;
        this.ceNode = node;
        this.childLogicalData = childData;
        Object.freeze(this);
    }
}
exports.default = LogicalData;
//# sourceMappingURL=logical.data.model.js.map