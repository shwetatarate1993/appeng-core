export default class APIRequestEntity {
    readonly name: string;
    readonly configId: string;
    readonly nodeId: string;
    readonly records: any[];
    readonly lePrimaryKey: string;
    readonly childEntities: APIRequestEntity[];
}
//# sourceMappingURL=api.request.entity.model.d.ts.map