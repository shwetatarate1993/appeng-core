import { LogicalData } from './';
export default class EntityGroupData {
    readonly logicalData: LogicalData;
    readonly referenceData: any;
    readonly action: string;
    constructor(logicalData: LogicalData, referenceData: any, action: string);
}
//# sourceMappingURL=entity.group.data.model.d.ts.map