"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class AppData {
    constructor(data) {
        this.isProcessingDone = false;
        this.data = data;
        Object.freeze(this);
    }
}
exports.default = AppData;
//# sourceMappingURL=app.data.model.js.map