import { Mode } from '../constant';
export default class DatabaseValidation {
    readonly configObjectId: string;
    readonly name: string;
    readonly configObjectType: string;
    readonly createdBy: string;
    readonly isDeleted: number;
    readonly itemDescription: string;
    readonly creationDate: Date;
    readonly projectId: number;
    readonly updatedBy: string;
    readonly updationDate: Date;
    readonly deletionDate: Date;
    readonly datasourceName: string;
    readonly validationType: string;
    readonly validationExpression: string;
    readonly validationQid: string;
    readonly validationMessage: string;
    readonly validationExpressionKeys: string;
    readonly mode: Mode;
    readonly isConditionAvailable?: boolean;
    readonly conditionExpression?: string;
    readonly privileges?: any[];
    readonly childRelations?: any[];
    readonly parentRelations?: any[];
    constructor(itemId: string, itemName: string, itemType: string, projectId: number, createdBy: string, itemDescription: string, creationDate: Date, updatedBy: string, updationDate: Date, deletionDate: Date, isDeleted: number, datasourceName: string, validationType: string, validationExpression: string, validationQid: string, validationMessage: string, validationExpressionKeys: string, privileges?: any[], childRelations?: any[], parentRelations?: any[]);
}
//# sourceMappingURL=databasevalidation.model.d.ts.map