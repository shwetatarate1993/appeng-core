export declare const DB: string;
export declare const LINKS: string;
export declare enum RequestMethod {
    GET = "GET",
    POST = "POST",
    PUT = "PUT",
    DELETE = "DELETE"
}
export declare enum Mode {
    INSERT = "INSERT",
    UPDATE = "UPDATE",
    BOTH = "BOTH"
}
export declare const MOCK = "mock";
export declare const tokenClaims: string[];
export declare const EXCEPTION_IN_PREPROCESSOR = "Custom Preprocessor Failed";
export declare const EXCEPTION_KEY = "__error";
export declare const SESSION_EXPIRED = "Session Expired";
//# sourceMappingURL=appeng.constant.d.ts.map