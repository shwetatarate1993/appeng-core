"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DB = 'db';
exports.LINKS = 'links';
var RequestMethod;
(function (RequestMethod) {
    RequestMethod["GET"] = "GET";
    RequestMethod["POST"] = "POST";
    RequestMethod["PUT"] = "PUT";
    RequestMethod["DELETE"] = "DELETE";
})(RequestMethod = exports.RequestMethod || (exports.RequestMethod = {}));
var Mode;
(function (Mode) {
    Mode["INSERT"] = "INSERT";
    Mode["UPDATE"] = "UPDATE";
    Mode["BOTH"] = "BOTH";
})(Mode = exports.Mode || (exports.Mode = {}));
exports.MOCK = 'mock';
exports.tokenClaims = ['iss', 'iat', 'exp', 'aud', 'sub'];
exports.EXCEPTION_IN_PREPROCESSOR = 'Custom Preprocessor Failed';
exports.EXCEPTION_KEY = '__error';
exports.SESSION_EXPIRED = 'Session Expired';
//# sourceMappingURL=appeng.constant.js.map