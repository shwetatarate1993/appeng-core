"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const info_commons_1 = require("info-commons");
const info_commons_2 = require("info-commons");
const jwt = __importStar(require("jsonwebtoken"));
exports.validateToken = async (token) => {
    if (!token) {
        const error = new info_commons_1.ErrorResponse();
        error.code = 401;
        error.message = 'Token is missing';
        error.errors = [{
                domain: 'global', reason: info_commons_2.ErrorReasonEnum.MISSING_TOKEN, message: 'Token is required', locationType: 'Request Header',
                location: info_commons_2.TOKEN_KEY, extendedHelp: 'http://request/help',
            }];
        return error;
    }
    let tokenResponse;
    try {
        tokenResponse = jwt.verify(token, info_commons_2.config.get(info_commons_2.SECRET));
    }
    catch (err) {
        const error = new info_commons_1.ErrorResponse();
        error.code = 401;
        error.message = 'Token is invalid';
        error.errors = [{
                domain: 'global', reason: info_commons_2.ErrorReasonEnum.INVALID_TOKEN, message: 'Please Send a Valid Token', locationType: 'Request Header',
                location: info_commons_2.TOKEN_KEY, extendedHelp: 'http://request/help',
            }];
        return error;
    }
    return tokenResponse;
};
//# sourceMappingURL=validate.token.js.map