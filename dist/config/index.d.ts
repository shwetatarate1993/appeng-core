import { default as convict } from 'convict';
declare const config: convict.Config<{
    provider: string;
    emailHost: string;
    emailPort: number;
    env: string;
    secret: string;
    ip: string;
    port: number;
    db: {
        mock: any;
        PRIMARYSPRING: any;
        PRIMARY_MD_SPRING: any;
        SPRING_INBOX_FM: any;
    };
    aws: {
        accessKey: any;
        secretKey: any;
        region: any;
        bucket: any;
    };
    links: {
        appeng_meta: any;
    };
}>;
export default config;
//# sourceMappingURL=index.d.ts.map