"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var api_request_1 = require("./api.request");
exports.apiRequestMock = api_request_1.default;
var header_token_1 = require("./header.token");
exports.MOKED_HEADER = header_token_1.default;
var data_preprocessor_request_1 = require("./data.preprocessor.request");
exports.dataPreprocessorRequestMock = data_preprocessor_request_1.default;
//# sourceMappingURL=index.js.map