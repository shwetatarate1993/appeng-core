"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {
    formData: {
        0: {
            child: {},
            APP_LOGGED_IN_USER_GP: 519,
            APP_LOGGED_IN_ROLE_ID: 17,
            APP_LOGGED_IN_YEAR: 1,
            APP_LOGGED_IN_USER_TALUKA: 27,
            APP_LOGGED_IN_USER_DISTRICT: 25,
            APP_LOGGED_IN_USER_STATE: 21,
            APP_LOGGED_IN_USER_ID: 1124,
            CLEAN_INDIA_MISSION_ID: null,
            FAMILIES_WITHOUT_TOILETS: 14,
            GRAMPANCHAYAT: 519,
            CLEAN_INDIA_MISSION_UUID: null,
            TOTAL_FAMILIES: '90',
            FAMILIES_HAVING_TOILETS: '76',
            TALUKA: '27',
            TOILET_BUILT_DURING_THE_WEEK: '',
        },
    },
    configId: '7f509518-d6d4-4952-99f8-ef93495945dd',
    configType: 'FormField',
};
//# sourceMappingURL=data.preprocessor.request.js.map